function parseMain($, momentd){

	function toDate(d){
		return moment(d, 'DD-MM-YYYY').unix()
	}

	function dates(d1, d2){
		if (d2) return [toDate(d1), toDate(d2)]
		return [toDate(d1)]
	}



	var under = {}
	$('div.forum--post-message').each(function( index ) {
  		// console.log( index + ": " + $( this ).html() );
  		if (index == 0){
  			//console.log( index + ": " + $( this ).html() );
  			var _in = 0
  			$( this ).html().split('\n').map(function(x){
  				if (!_in && x.match(/Список предупрежденных/)) _in = 1
  				else {
  					var a = x.match(/^\s*?<b>\s*?(.*?)\s*?<\/b>\s*?-*\s*?(\d{1,2}.\d{1,2}\.\d{2,4})(?: ?, ?(\d{1,2}.\d{1,2}\.\d{2,4}))?(?:<br>)/)
  					if (a){
  						// console.log(a)
  						var x = {
  							login : a[1],
  							warn : dates(a[2],a[3])
  						}
  						under[x.login] = x
  					}
  				}
  			})

  			console.log(under);
  		}

	});

}