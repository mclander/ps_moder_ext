// Print all of the news items on hackernews
var fs     = require('fs');
var jsdom  = require('jsdom');
var jquery = fs.readFileSync("./jquery.js").toString();

jsdom.env({
   html: 'http://news.ycombinator.com/',
   src: [
      jquery
   ],
   done: function(errors, window) {
      var $ = window.$;
      console.log('HN Links');
      $('td.title:not(:last) a').each(function() {
         console.log(' -', $(this).text());
      });
   }
});
